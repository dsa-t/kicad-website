+++
title = "Driverino-Shield"
projectdeveloper = "Michele Santucci"
projecturl = "https://github.com/weirdgyn/Driverino-Shield"
"made-with-kicad/categories" = [
    "Motor-Controller"
]
+++

Driverino-Shield is a simple, low power, controller for BLDC sensored
motors.  The project target was to create an Arduino shield capable of
driving a PM BLDC motor rated about 50-100W.  The board is fitted with
TI MCT8316Z BLDC driver.
