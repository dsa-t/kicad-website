+++
title = "Windows Downloads"
distro = "Windows"
summary = "Downloads KiCad for Windows 8.1, Windows 10 and Windows 11"
iconhtml = "<div><i class='fab fa-windows'></i></div>"
weight = 2
+++

[.initial-text]
KiCad supports Windows 10 and 11.  See
link:/help/system-requirements/[System Requirements] for more details.

[.initial-text]
== Stable Release

Current Version: *{{< param "release" >}}*

++++
<div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
	<div class="accordion-item">
		<div class="accordion-header" role="tab" id="mirrors-64bit-heading">
			<button role="button" class="accordion-button" data-bs-toggle="collapse" data-parent="#accordion" href="#mirrors-64bit" aria-expanded="true" aria-controls="mirrors-64bit">
					64-bit (recommended)
			</button>
		</div>
		<div id="mirrors-64bit" class="accordion-collapse collapse show" role="tabpanel" aria-labelledby="mirrors-64bit-heading">
			<div class="accordion-body">
				<h4>Worldwide</h4>
				<div class="list-group download-list-group">
					<a class="list-group-item dl-link osdn" href="https://osdn.net/projects/kicad/storage/kicad-{{< param "release" >}}-x86_64.exe">
						<img src="//osdn.net/sflogo.php?group_id=12159&type=1" width="96" height="31"  border="0" alt="OSDN"> OSDN
					</a>
					<a class="list-group-item dl-link" href="https://github.com/KiCad/kicad-source-mirror/releases/download/{{< param "release" >}}/kicad-{{< param "release" >}}-x86_64.exe"">
						<img src="/img/download/github.svg" height="31" /> GitHub
					</a>
				</div>
				<h4>Europe</h4>
				<div class="list-group download-list-group">
					<a class="list-group-item dl-link" href="https://kicad-downloads.s3.cern.ch/windows/stable/kicad-{{< param "release" >}}-x86_64.exe">
						<img src="/img/about/cern-logo.png" /> CERN - Switzerland
					</a>
					<a class="list-group-item dl-link" href="https://www2.futureware.at/~nickoe/kicad-downloads-mirror/windows/stable/kicad-{{< param "release" >}}-x86_64.exe">
						Futureware - Austria
					</a>
				</div>
				<h4>Asia</h4>
				<div class="list-group download-list-group">

					<a class="list-group-item dl-link" href="https://mirrors.aliyun.com/kicad/windows/stable/kicad-{{< param "release" >}}-x86_64.exe">
						<img src="/img/download/aliyun.png" border="0" alt="AliCloud" />AlibabaCloud
					</a>
					<a class="list-group-item dl-link" href="https://mirrors.cqu.edu.cn/kicad/windows/stable/kicad-{{< param "release" >}}-x86_64.exe">
						<img src="/img/download/chongqing.jpeg" />Chongqing University
					</a>
					<a class="list-group-item dl-link" href="https://mirror.tuna.tsinghua.edu.cn/kicad/windows/stable/kicad-{{< param "release" >}}-x86_64.exe">
						<img src="/img/download/tuna.png" />Tsinghua University
					</a>
				</div>
				<div class="list-group download-list-group">
					<h4>Australia</h4>
					<a class="list-group-item dl-link" href="https://mirror.aarnet.edu.au/pub/kicad/windows/stable/kicad-{{< param "release" >}}-x86_64.exe">
						<img src="/img/download/aarnet_logo_white.svg" height="31" > AARNet
					</a>
				</div>
			</div>
		</div>
	</div>

	<div class="accordion-item">
		<div class="accordion-header" role="tab" id="mirrors-32bit-heading">
			<button role="button" class="accordion-button" data-bs-toggle="collapse" data-parent="#accordion" href="#mirrors-32bit" aria-expanded="false" aria-controls="mirrors-32bit">
					32-bit
			</button>
		</div>
		<div id="mirrors-32bit" class="accordion-collapse collapse" role="tabpanel" aria-labelledby="mirrors-32bit-heading">
			<div class="accordion-body">
				<h4>Worldwide</h4>
				<div class="list-group download-list-group">
					<a class="list-group-item dl-link osdn" href="https://osdn.net/projects/kicad/storage/kicad-{{< param "release" >}}-i686.exe">
						<img src="//osdn.net/sflogo.php?group_id=12159&type=1" width="96" height="31"  border="0" alt="OSDN"> OSDN
					</a>
					<a class="list-group-item dl-link" href="https://github.com/KiCad/kicad-source-mirror/releases/download/{{< param "release" >}}/kicad-{{< param "release" >}}-i686.exe"">
						<img src="/img/download/github.svg" height="31" /> GitHub
					</a>
				</div>
				<h4>Europe</h4>
				<div class="list-group download-list-group">
					<a class="list-group-item dl-link" href="https://kicad-downloads.s3.cern.ch/windows/stable/kicad-{{< param "release" >}}-i686.exe">
						<img src="/img/about/cern-logo.png" /> CERN - Switzerland
					</a>
					<a class="list-group-item dl-link" href="https://www2.futureware.at/~nickoe/kicad-downloads-mirror/windows/stable/kicad-{{< param "release" >}}-i686.exe">
						Futureware - Austria
					</a>
				</div>
				<h4>Asia</h4>
				<div class="list-group download-list-group">
					<a class="list-group-item dl-link" href="https://mirrors.aliyun.com/kicad/windows/stable/kicad-{{< param "release" >}}-i686.exe">
						<img src="/img/download/aliyun.png" border="0" alt="AliCloud" /> AlibabaCloud
					</a>
					<a class="list-group-item dl-link" href="https://mirrors.cqu.edu.cn/kicad/windows/stable/kicad-{{< param "release" >}}-i686.exe">
						<img src="/img/download/chongqing.jpeg" /> Chongqing University
					</a>
					<a class="list-group-item dl-link" href="https://mirror.tuna.tsinghua.edu.cn/kicad/windows/stable/kicad-{{< param "release" >}}-i686.exe">
						<img src="/img/download/tuna.png" />Tsinghua University
					</a>
				</div>
				<div class="list-group download-list-group">
					<h4>Australia</h4>
					<a class="list-group-item dl-link" href="https://mirror.aarnet.edu.au/pub/kicad/windows/stable/kicad-{{< param "release" >}}-i686.exe">
						<img src="/img/download/aarnet_logo_white.svg" height="31" > AARNet
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
++++

[.donate-hidden]
== {nbsp}
++++
	{{< getpartial "download_thanks.html" >}}
++++



== Download Verification
All installer binaries will have a code signed digital signature attached. Windows will
automatically verify the signature is valid, however you may want to ensure it actually is
present and the correct signer. A guide on how to verify the installer is available here:
link:/help/windows-download-verification/[Windows Installer Verification Guide]

A valid official KiCad signature has one of these certificate infos:

[role="table table-striped table-condensed"]
|===
|Signer Name|*KICAD SERVICES CORPORATION*
|Issuer|*GlobalSign GCC R45 EV CodeSigning CA 2020*
|Serial Number|*3e1be0c88cf5ab57cbfcae27*
|===

[role="table table-striped table-condensed"]
|===
|Signer Name|*KiCad Services Corporation*
|Issuer|*Sectigo RSA Code Signing CA*
|Serial Number|*1f70b098b5c21a254a6fb427cdf8893e*
|===


== Previous Releases

Previous releases should be available for download on:

https://downloads.kicad.org/kicad/windows/explore/stable


== Testing Builds

The _testing_ builds are snapshots of the current stable release codebase at a specific time.
These contain the most recent changes that will be included in the next bugfix release in the
current stable series.  For example, if the current stable release is 7.0.0, these builds will
contain changes destined for version 7.0.1.

https://downloads.kicad.org/kicad/windows/explore/6.0-testing


== Nightly Development Builds

The _nightly development_ builds are snapshots of the development (master branch) codebase at a
specific time.  This codebase is under active development, and while we try our best, may contain
more bugs than usual.  New features added to KiCad can be tested in these builds.

WARNING: Please read link:/help/nightlies-and-rcs/[Nightly Builds and Release Candidates] for
		 important information about the risks and drawbacks of using nightly builds.

https://downloads.kicad.org/kicad/windows/explore/nightlies
